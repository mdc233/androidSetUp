* 使用AVDs(Android虚拟设备)
    * 对于仿真器而言，你的电脑将留出一块内存去复制基于仿真器正在仿真设备上的环境。Android Studio使用仿真器，这意味着Android虚拟设备管理器启动一个Linux内核的大沙箱和整个Android栈为了仿真基于Android物理设备的环境。所以， **优先直接使用Android物理设备** 。
    * 创建步骤:<br>
    点击Android虚拟设备管理器图标(右上角) -> 创建虚拟设备 -> 选择Galaxy Nexus -> 选择系统镜像(需要联网且下载)，选择第一个选项Lollopop（或最新的API）和x86_64的ABI -> 在下一个界面上，点击Finish按钮来验证你的AVD设置。恭喜你，你已经创建了一个新的AVD。
    * 使用步骤 -> 运行&选择AVDs（第一次需要下载平台android-22） -> 运行失败
        * 错误 -> Emulator: emulator: ERROR: Unknown AVD name [Galaxy_Nexus_API_22], use -list-avds to see valid list.
        * 错误原因: 有两个环境变量ANDROID_EMULATOR_HOME和ANDROID_AVD_HOME，它们默认位置分别是$ANDROID_SDK_HOME/.android/和$ANDROID_SDK_HOME/.android/avd/，  **但是我的sdk目录下却没有.android。** <br>
          对AVDs进行view detail后发现其地址： C:\Users\wwwmi\.android\avd\Galaxy_Nexus_API_22.avd
        * 两种尝试的方法：
            * 1.重建一个AVDs，设置安装位置在sdk目录下。
            * 2.设置$ANDROID_EMULATOR_HOME, 并创建/avd
                * 由于重建时没发现选安装位置的地方，所以直接尝试方法二。
            * 尝试后出现错误二。（错误一似乎是解决了）
        * 错误二 -> Emulator: emulator: ERROR: x86 emulation currently requires hardware acceleration!<br>
                    Emulator: Warning: Quick Boot / Snapshots not supported on this machine. A CPU with EPT + UG features is currently needed. We will address this in a future release.
        * 解决方案：<br>
        1.Open SDK Manager and Download Intel x86 Emulator Accelerator (HAXM installer) if you haven't.(我并没有下载)
            * 成功！！！ -> helloAndroid也运行成功！！！

* 使用物理设备(没带数据线，需回去验证)
    * 访问设备USB驱动程序[https://developer.android.com/studio/run/oem-usb]
    * 在Android设备上，点击设置，然后打开开发人员选项。确保USB调试检查框被选中,打开你设备上的USB调试开关。
    * 没找到相关查看按钮，到时具体操作时再找。
    * 注意：你的电脑和你的Android设备之间的连接是使用了一种叫做Android调试桥（ADB）的服务。如果你没有看到设备，点击IDE左下角的Terminal按钮，输入以下命令：`adb start-server`,来重启ADB服务。
    * 运行程序的操作过程与AVDs相同。


* 熟悉AS的导航系统(操作页面)
    * the editor: 中心位置的编辑器，围绕在编辑器周围的其他窗口都被称为工具窗口(tool windows)和侧窗口(cluster)（左部，底部，右部）。
        * 可以从工具窗口拖拽文件到编辑器，这样做可以直接作为选项卡在编辑器中打开。

![输入图片说明](https://images.gitee.com/uploads/images/2018/0927/193539_459a03ab_2162412.jpeg "AS导航.jpg")

    * Editor tabs(选项卡): 右键选项卡可以进行各种操作


（没整理的上课笔记）<br>
* 基本概述：
    * activity 即UI的容器，窗口。多个activity构成了程序。（也是父类）
    * intents 用于各部件通信，广播，启动activity，用于构成android的核心消息系统。
        * 注册intent接收器，以监听intent消息。
        * android如果没找到匹配的接收器就会崩溃
    * 异步调用AsyncTask类
    * 后台服务
    * 提供的硬件工具
    * 软件服务
* 基本原则：keep it simple, Stupid(调用已有的东西来构建程序)
* apk文件: Android上的安装文件，相当于zip。
* Java命名的规则
* AndroidManifest.xml: 应用和操作系统的桥梁
* Java目录，放java代码的
* Res放用到的资源
* Assets目录放多媒体
* 配置文件->叫醒
* activity->oncreate()->setContentView()按定义好的布局显示界面。
* 布局文件main.xml只定义形式，内容在其他地方。
    * string.hello在res/strings.xml
* 界面编程
    * Framework -> view System
    * 布局->xml（直观可读/界面和逻辑代码分离） or java（动态布局/重要代码保密）
    * 布局->ConstraintLayout（google主推）
    * layout_gravity(相较于整体布局) 与 gravity（局部内）
    * 去官网看下各种API怎么用
    * 帧布局Framelayout
    * Thursday/自己学号(全英文)的目录下新建程序
    * grade改阿里云镜像会快很多！！！