# Android开发环境搭建
* 查看以及配置java环境

```
C:\Users\wwwmi>java
'java' 不是内部或外部命令，也不是可运行的程序
或批处理文件。
C:\Users\wwwmi>javac
'javac' 不是内部或外部命令，也不是可运行的程序
或批处理文件。
C:\Users\wwwmi>javadoc
'javadoc' 不是内部或外部命令，也不是可运行的程序
或批处理文件。
C:\Users\wwwmi>java -version
'java' 不是内部或外部命令，也不是可运行的程序
或批处理文件。
（刚换电脑，连JDK都没安装）
```
1.按菜鸟教程上的下载安装以及配置环境变量<br>
>JDK是整个java开发的核心，它包含了JAVA的运行环境（JVM+Java系统类库）和JAVA工具。<br>
>JRE是Java Runtime Environment缩写，指Java运行环境，是运行JAVA程序所必须的环境的集合，包含JVM标准实现及Java核心类库。<br>
    
* 记住安装目录：(这次)C:\Program Files\Java\jdk-10.0.2\

```
C:\Users\wwwmi>java -version
java version "10.0.2" 2018-07-17
Java(TM) SE Runtime Environment 18.3 (build 10.0.2+13)
Java HotSpot(TM) 64-Bit Server VM 18.3 (build 10.0.2+13, mixed mode)

C:\Users\wwwmi>java
有反应

C:\Users\wwwmi>javac
'javac' 不是内部或外部命令，也不是可运行的程序
或批处理文件。
```
* 为何javac出了问题？
    * 在 Windows10 中，Path 变量里是分条显示的，我们需要将 %JAVA_HOME%\bin;%JAVA_HOME%\jre\bin; 分开添加，否则无法识别：（依旧不行）
    * 重启试试（成功）

```
C:\Users\wwwmi>java -version
java version "10.0.2" 2018-07-17
Java(TM) SE Runtime Environment 18.3 (build 10.0.2+13)
Java HotSpot(TM) 64-Bit Server VM 18.3 (build 10.0.2+13, mixed mode)

C:\Users\wwwmi>javac -version
javac 10.0.2
```

* AS配置：（注意：国外网站用Google搜，百度太垃圾了）
    * 按步骤安装，然后设置socks代理：主机号：127.0.0.1 端口：1080（即ss代理端口）
    * 重启，自动下载sdk（软件开发工具包）